<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload extends CI_Controller
{

	public function construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_HandleFile');
	}

	public function index()
	{
		$this->load->model('M_HandleFile');
		$model = $this->M_HandleFile;
		$file = $model->get_data();
		$data = [
			'file' => $file,
			'error' => '',
		];
		$this->load->view('upload', $data);
	}

	public function upload_file()
	{
		// Set path dan rule file
		$config['upload_path'] = 'public/uploads/';
		$config['allowed_types'] = 'png|jpg|jpeg|gif';
		$config['max_size'] = 2048;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('imageFile')) {
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);
		} else {
			// Upload PDFs
			$filePdf = $_FILES['pdfFiles'];
			$uploadData = array();

			// Menyimpan data gambar
			$uploaded_image_data = $this->upload->data();
			$imageData = array(
				'path' => 'public/uploads/' . $uploaded_image_data['file_name'],
				'file_type' => $uploaded_image_data['file_type']
			);
			array_push($uploadData, $imageData);

			foreach ($filePdf['name'] as $key => $pdfFile) {
				$_FILES['pdfFile']['name'] = $filePdf['name'][$key];
				$_FILES['pdfFile']['type'] = $filePdf['type'][$key];
				$_FILES['pdfFile']['tmp_name'] = $filePdf['tmp_name'][$key];
				$_FILES['pdfFile']['error'] = $filePdf['error'][$key];
				$_FILES['pdfFile']['size'] = $filePdf['size'][$key];

				$config['upload_path'] = 'public/uploads/';
				$config['allowed_types'] = 'pdf';
				$config['max_size'] = 2048;

				$this->upload->initialize($config);

				if ($this->upload->do_upload('pdfFile')) {
					$uploaded_data = $this->upload->data();
					$file_data = array(
						'path' => 'public/uploads/' . $uploaded_data['file_name'],
						'file_type' => $uploaded_data['file_type']
					);
					array_push($uploadData, $file_data);
				}
			}

			if (!empty($uploadData)) {
				$this->load->model('M_HandleFile');

				$simpanData = $this->M_HandleFile->insert_files($uploadData);

				if (!empty($simpanData)) {
					$data = array('upload_data' => $simpanData);
					$this->session->set_flashdata('success', 'File berhasil diupload');
					redirect('upload');
				} else {
					$error = array('error' => 'Gagal memasukkan data ke database');
					redirect('upload');
				}
			} else {
				$error = array('error' => 'Gagal mengunggah file PDF');
				redirect('upload');
			}
		}
	}


	public function delete($fileId)
	{
		// Mendapatkan path file berdasarkan ID
		$fileData = $this->db->where('id', $fileId)->get('upload_file')->row();
		$filePath = $fileData->path;

		// Menghapus file dari sistem file
		if (file_exists($filePath)) {
			unlink($filePath);
		}

		$this->db->where('id', $fileId);
		$this->db->delete('upload_file');
		$this->session->set_flashdata('success', 'File berhasil dihapus');

		redirect('upload');
	}
}
