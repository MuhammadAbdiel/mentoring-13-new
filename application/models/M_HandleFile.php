<?php
class M_HandleFile extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_data()
    {
        $query = $this->db->get('upload_file');
        return $query->result_array();
    }

    public function insert_files($uploadData)
    {
        $simpanFile = [];
        foreach ($uploadData as $data) {
            $data = array(
                'path' => $data['path'],
                'file_type' => $data['file_type']
            );

            $this->db->insert('upload_file', $data);

            if ($this->db->affected_rows() > 0) {
                array_push($simpanFile, $data);
            }
        }

        return $simpanFile;
    }
}
