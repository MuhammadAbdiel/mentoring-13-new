<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="container mt-5">
        <div class="col-12">
            <?php if ($this->session->flashdata('success')) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?= $this->session->flashdata('success') ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php endif; ?>

            <form action="<?= base_url('upload/upload_file') ?>" method="post" enctype="multipart/form-data" class="d-flex row">
                <div class="col-12 mb-3">
                    <label for="imageFile" class="fw-bold">JPG|PNG|JPEG</label>
                    <input type="file" class="form-control" name="imageFile" accept=".png, .jpg, .jpeg">
                </div>
                <div class="col-12">
                    <label for="pdfFiles" class="fw-bold">PDF</label>
                    <input type="file" class="form-control" name="pdfFiles[]" accept=".pdf" multiple>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-success mt-3">Submit</button>
                </div>
            </form>
        </div>

        <table class="table table-bordered mt-5">
            <thead>
                <th>No</th>
                <th>Gambar</th>
                <th>Path</th>
                <th>Tipe File</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                <?php
                $nomor = 1;
                foreach ($file as $data) :
                ?>
                    <tr>
                        <td>
                            <?= $nomor++ ?>
                        </td>
                        <td class="w-25">
                            <img src="../<?= $data['path'] ?>" class="w-25 h-25 img-fluid" alt="">
                        </td>
                        <td>
                            <?= $data['path'] ?>
                        </td>
                        <td>
                            <?= substr($data['file_type'], strpos($data['file_type'], '/') + 1) ?>
                        </td>
                        <td><a href="#" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $data['id'] ?>" class="btn btn-danger btn-sm">Delete</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    </div>
</body>

</html>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<?php
foreach ($file as $data) : ?>
    <div class="modal fade" id="deleteModal<?= $data['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus data ini?
                    <form action="<?= base_url('upload/delete/' . $data['id']) ?>" method="post">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>